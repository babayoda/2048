﻿/*  x,y coordinate where x is vertical and y is horizontal.
    up      x-1
    down    x+1
    left    y-1
    right   y+1
 */
namespace game {
    using UnityEngine;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using input;

    public class Board : MonoBehaviour
    {
        public event Action newMove;
        public RectTransform board;
        public Tile tile_prefab;
        public Transform pool_transform;
        public float scale = 1;
        public int level { get { return _level; }}
        public float anim_coef = 10;
        IInput _input;
        List<Tile> _board = new List<Tile>();
        Stack<Tile> _pool_stack = new Stack<Tile>();
        int _level = 4;
        float _boardWidth, _tileWidth;
        
        void Start() {
            Init();
            NewGame();
        }

        void Update() {
            if(Input.GetKeyDown(KeyCode.N))
                NewGame();
        }

        void Init() {
            _boardWidth = board.rect.width;
            _tileWidth = _boardWidth/_level;
            scale = board.rect.width/((float)_level*100f);
            for(int i=0; i<_level; i++) {
                for(int j=0; j<_level; j++){
                    Tile t = Instantiate<Tile>(tile_prefab, pool_transform);
                    _pool_stack.Push(t);

                }
            }
            _input = gameObject.AddComponent<SwipeDirection>();
            _input.swipe += Move;
        }

        List<Vector2Int> RandomPositions(int n, int range) {
            List<Vector2Int> positions = new List<Vector2Int>();
            for(int i = 0; i<n; i++) {
                Vector2Int v = new Vector2Int(UnityEngine.Random.Range(0, range), UnityEngine.Random.Range(0, range));
                while(positions.Any(x=>x.x == v.x && x.y == v.y)) {
                    v.x = v.x + 1;
                    if(v.x >= range) {
                        v.x = 0; 
                        v.y = (v.y+1)%range;
                    }
                }
                positions.Add(v);
            }
            return positions;
        }

        public void NewGame() {
            foreach(var v in _board) Kick(v);
            var pos = RandomPositions(4, _level);
            for(int i = 0; i<pos.Count; i++) {
                var t = _pool_stack.Pop();
                t.transform.name = "Tile_" + (i+1).ToString();
                _board.Add(t);
                t.Init(this, 2, Color.red, pos[i], _input);
            }
        }

        void NewTile(Vector2Int pos, int val)
        {
            var t = _pool_stack.Pop();
            t.transform.name = "Tile_" + val.ToString();
            _board.Add(t);
            t.Init(this, val, Color.red, pos, _input);
        }

        public void Kick(Tile tile) {
            if(_board.Remove(tile)) {
                tile.gameObject.SetActive(false);
                tile.transform.SetParent(pool_transform);
                _pool_stack.Push(tile);
            }
        }

        public Tile GetNext(Tile tile, Direction direction)
        {
            Tile next = null;
            switch(direction) {
                case Direction.UP:
                    foreach(var x in _board) {
                        next = (x.alive
                                && x.pos.y == tile.pos.y
                                && x.pos.x < tile.pos.x
                                && ( next == null || next.pos.x < x.pos.x )
                            ) ? x : next;
                    }
                    break;
                case Direction.DOWN:
                    foreach(var x in _board) {
                        next = (x.alive
                                && x.pos.y == tile.pos.y
                                && x.pos.x > tile.pos.x
                                && ( next == null || next.pos.x > x.pos.x )
                            ) ? x : next;
                    }
                    break;
                case Direction.LEFT:
                    foreach(var x in _board) {
                        next = (x.alive
                                && x.pos.x == tile.pos.x
                                && x.pos.y < tile.pos.y
                                && ( next == null || next.pos.y < x.pos.y )
                            ) ? x : next;
                    }
                    break;
                case Direction.RIGHT:
                    foreach(var x in _board) {
                        next = (x.alive
                                && x.pos.x == tile.pos.x
                                && x.pos.y > tile.pos.y
                                && ( next == null || next.pos.y > x.pos.y )
                            ) ? x : next;
                    }
                    break;
            }
            return next;
        }

        // push
        void Move(Direction direction) {
            newMove?.Invoke();
            Tile[] edge_tiles = new Tile[_level];
            int u;
            for(int i=0; i<_board.Count; i++) {
                switch(direction) {
                    case Direction.UP:
                        u = _board[i].pos.y;
                        if(edge_tiles[u] == null || edge_tiles[u].pos.x < _board[i].pos.x)
                            edge_tiles[u] = _board[i];
                        break;
                    case Direction.DOWN:
                        u = _board[i].pos.y;
                        if(edge_tiles[u] == null || edge_tiles[u].pos.x > _board[i].pos.x)
                            edge_tiles[u] = _board[i];
                        break;
                    case Direction.LEFT:
                        u = _board[i].pos.x;
                        if(edge_tiles[u] == null || edge_tiles[u].pos.y < _board[i].pos.y)
                            edge_tiles[u] = _board[i];
                        break;
                    case Direction.RIGHT:
                        u = _board[i].pos.x;
                        if(edge_tiles[u] == null || edge_tiles[u].pos.y > _board[i].pos.y)
                            edge_tiles[u] = _board[i];
                        break;
                }
            }
            foreach(var v in edge_tiles) if(v) v.Push(direction);
            NewTileAfterMovement(direction);
        }

        void NewTileAfterMovement(Direction direction)
        {
            // New tile
            int?[] p = new int?[_level];
            int u;
            for(int i=0; i<_board.Count; i++) {
                switch(direction) {
                    case Direction.UP:
                        u = _board[i].targetPos.y;
                        if(p[u] == null || p[u] < _board[i].targetPos.x)
                            p[u] = _board[i].targetPos.x;
                        break;
                    case Direction.DOWN:
                        u = _board[i].targetPos.y;
                        if(p[u] == null || p[u] > _board[i].targetPos.x)
                            p[u] = _board[i].targetPos.x;
                        break;
                    case Direction.LEFT:
                        u = _board[i].targetPos.x;
                        if(p[u] == null || p[u] < _board[i].targetPos.y)
                            p[u] = _board[i].targetPos.y;
                        break;
                    case Direction.RIGHT:
                        u = _board[i].targetPos.x;
                        if(p[u] == null || p[u] > _board[i].targetPos.y)
                            p[u] = _board[i].targetPos.y;
                        break;
                }
            }
            int x = 0, y = 0;
            switch(direction) {
                case Direction.UP:
                    y = UnityEngine.Random.Range(0, _level);
                    if(p[y]!=null && p[y]>=(_level-1)) {
                        int start = (y+1)%_level;
                        while(p[start]!=null && start!=y && p[start]>=(_level-1))
                            start = (start+1)%_level;
                        if(start != y)
                            y = start;
                    }
                    if(p[y] == null) {
                        x = UnityEngine.Random.Range(0, _level);
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else if(p[y] < (_level-1)){
                        x = UnityEngine.Random.Range((p[y]??default(int))+1, _level);
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else
                        GameOver();
                    break;
                case Direction.DOWN:
                    y = UnityEngine.Random.Range(0, _level);
                    if(p[y]!=null && p[y]<=0) {
                        int start = (y+1)%_level;
                        while(p[start]!=null && start!=y && p[start]<=0)
                            start = (start+1)%_level;
                        if(start != y)
                            y = start;
                    }
                    if(p[y] == null) {
                        x = UnityEngine.Random.Range(0, _level);
                        NewTile(new Vector2Int(x, y), 2);
                    } 
                    else if (p[y] > 0){
                        x = UnityEngine.Random.Range(0, p[y]??default(int));
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else
                        GameOver();
                    break;
                case Direction.LEFT:
                    x = UnityEngine.Random.Range(0, _level);
                    if(p[x]!=null && p[x]>=(_level-1)) {
                        int start = (x+1)%_level;
                        while(p[start]!=null && start!=x && p[start]>=(_level-1))
                            start = (start+1)%_level;
                        x = start;
                    }
                    if(p[x] == null) {
                        y = UnityEngine.Random.Range(0, _level);
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else if(p[x] < (_level-1)){
                        y = UnityEngine.Random.Range((p[x]??default(int))+1, _level);
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else
                        GameOver();
                    break;
                case Direction.RIGHT:
                    x = UnityEngine.Random.Range(0, _level);
                    if(p[x]!=null && p[x]<=0) {
                        int start = (x+1)%_level;
                        while(p[start]!=null && start!=x && p[start]<=0)
                            start = (start+1)%_level;
                        x = start;
                    }
                    if(p[x] == null) {
                        y = UnityEngine.Random.Range(0, _level);
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else if (p[x] > 0) {
                        y = UnityEngine.Random.Range(0, p[x]??default(int));
                        NewTile(new Vector2Int(x, y), 2);
                    }
                    else
                        GameOver();
                    break;
            }
        }

        void GameOver()
        {
            Debug.Log("Game over");
        }
    }
}
