﻿namespace  game
{
    using UnityEngine;
    using UnityEngine.UI;
    using input;

    public class Tile : MonoBehaviour
    {
        public float threshold_distance = 0.1f;
        public Text num_text;
        ///<summary>Current 2D index.</summary>
        public Vector2Int pos { get { return _current_index; }}
        public Vector2Int targetPos { get { return _target_index; }}
        public bool alive { get { return _val > 0; }}
        Vector2Int _current_index;      // current 2D index
        Vector2Int _target_index;
        Vector2 _tempPos;
        Vector2 _target_pos;
        int _val;
        Color _color;
        Board _board;
        bool _move = false;
        Image _image;

        void Awake() {
            _image = GetComponent<Image>();
        }

        void Update() {
            if(_move) {
                _tempPos = Vector2.Lerp(_tempPos, _target_pos, Time.deltaTime*_board.anim_coef);
                if(Vector2.Distance(_tempPos, _target_pos) < threshold_distance) {
                    UpdatePositionQuick();
                } else
                    transform.localPosition = _tempPos;
            }
        }

        public void Init(Board board, int value, Color color, Vector2Int position, IInput input) {
            _color = color;
            if(_image == null) _image = GetComponent<Image>();
            _image.color = color;
            _val = value;
            num_text.text = value.ToString();
            _board = board;
            _board.newMove += UpdatePositionQuick;
            _target_index = _current_index = position;
            transform.SetParent(board.transform);
            transform.localScale = Vector3.one*_board.scale;
            gameObject.SetActive(true);
            SetTargetPosition();
            UpdatePositionQuick();
        }

        public int Push(Direction direction) {
            UpdatePositionQuick();
            Tile next = _board.GetNext(this, direction);
            if(!next) {
                SetPosition(next, direction);
                return _val;
            } else {
                int val = next.Push(direction);
                if(val == _val) {
                    next.EndHasCometh();
                    _val *= 2;
                    next = _board.GetNext(this, direction);
                    SetPosition(next, direction);
                    return 0;
                } else {
                    SetPosition(next, direction);
                    return _val;
                }
            }
        }

        void SetPosition(Tile tile, Direction motionDirection) {
            if(!tile) {
                switch(motionDirection) {
                    case Direction.UP:
                        if(_target_index.x > 0) {
                            _move = true;
                            _target_index.x = 0;
                        }
                        break;
                    case Direction.DOWN:
                        if(_target_index.x < _board.level-1) {
                            _move = true;
                            _target_index.x = _board.level-1;
                        }
                        break;
                    case Direction.LEFT:
                        if(_target_index.y > 0) {
                            _move = true;
                            _target_index.y = 0;
                        }
                        break;
                    case Direction.RIGHT:
                        if(_target_index.y < _board.level-1) {
                            _move = true;
                            _target_index.y = _board.level-1;
                        }
                        break;
                }
            } else {
                switch(motionDirection) {
                    case Direction.UP:
                        _target_index = tile._target_index+(new Vector2Int(1, 0));
                        _move = true;
                        break;
                    case Direction.DOWN:
                        _target_index = tile._target_index+(new Vector2Int(-1, 0));
                        _move = true;
                        break;
                    case Direction.LEFT:
                        _target_index = tile._target_index+(new Vector2Int(0, 1));
                        _move = true;
                        break;
                    case Direction.RIGHT:
                        _target_index = tile._target_index+(new Vector2Int(0, -1));
                        _move = true;
                        break;
                }
            }
            SetTargetPosition();
        }

        void UpdatePositionQuick()
        {
            if(_val == 0) {
                _board.newMove -= UpdatePositionQuick;
                _board.Kick(this);
            }
            else {
                num_text.text = _val.ToString();
                _current_index = _target_index;
                transform.localPosition = _tempPos = _target_pos;
                _move = false;
            }
        }

        void SetTargetPosition() {
            _target_pos = ((new Vector2(_target_index.y, _board.level-1-_target_index.x))*_board.scale*100f - Vector2.one*_board.scale*150f);
        }

        public void EndHasCometh() {
            transform.SetSiblingIndex(0);
            _val = 0;
        }
    }
}
