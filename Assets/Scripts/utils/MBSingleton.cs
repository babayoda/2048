﻿using UnityEngine;

namespace utils {
    public class MBSingleton<T> : MonoBehaviour
        where T:MBSingleton<T>
    {
        static T _instance;
        public static T instance {
            get {
                if(_instance == null)
                    (new GameObject(typeof(T).ToString())).AddComponent<T>();
                return _instance;
            }
        }
        protected virtual void Awake() {
            if(_instance == null) {
                _instance = this as T;
                DontDestroyOnLoad(gameObject);
            } else Destroy(this);
        }
    }
}
