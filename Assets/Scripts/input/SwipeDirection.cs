﻿
namespace input {
    using UnityEngine;
    using UnityEngine.EventSystems;
    using System;
    using game;

    public class SwipeDirection : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IInput
    {
        public event Action<Direction> swipe;
        Vector2 _startPos;
        float _threshold = -1;
        
        public Vector2 _topLeft;
        public Vector2 _bottomLeft;
        public Vector2 _bottomRight;
        public Vector2 _topRight;

        void Awake() {
            _threshold = Screen.dpi/4;
            // swipe+=Swipe;
        }

        void Update() {
            if(Input.GetKeyDown(KeyCode.A))
                swipe?.Invoke(Direction.LEFT);
            if(Input.GetKeyDown(KeyCode.S))
                swipe?.Invoke(Direction.DOWN);
            if(Input.GetKeyDown(KeyCode.D))
                swipe?.Invoke(Direction.RIGHT);
            if(Input.GetKeyDown(KeyCode.W))
                swipe?.Invoke(Direction.UP);
        }

        void Swipe(Direction dir) {
            Debug.Log("Swipe " + dir.ToString());
        }

        void OnMouseDown() {
            _startPos = Input.mousePosition;
        }
        
        void OnMouseUp() {
            var delta = (Vector2)Input.mousePosition - _startPos;
            if(Mathf.Abs(delta.x) > Mathf.Abs(delta.y)) {
                if(Mathf.Abs(delta.x) > _threshold)
                    swipe?.Invoke(delta.x > 0 ? Direction.RIGHT : Direction.LEFT);
            } else {
                if(Mathf.Abs(delta.y) > _threshold)
                    swipe?.Invoke(delta.y > 0 ? Direction.UP : Direction.DOWN);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _startPos = Input.mousePosition;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            var delta = (Vector2)Input.mousePosition - _startPos;
            if(Mathf.Abs(delta.x) > Mathf.Abs(delta.y)) {
                if(Mathf.Abs(delta.x) > _threshold)
                    swipe?.Invoke(delta.x > 0 ? Direction.RIGHT : Direction.LEFT);
            } else {
                if(Mathf.Abs(delta.y) > _threshold)
                    swipe?.Invoke(delta.y > 0 ? Direction.UP : Direction.DOWN);
            }
        }
    }
}
